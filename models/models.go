package models

import "time"

type Model struct{
	ID 		uint 	`gorm:"primaryKey;autoIncrement:true" json:"id"`
	CreatedAt time.Time
	UpdatedAt time.Time
}