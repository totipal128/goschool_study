package seed

import (
	"encoding/json"
	"golangSekolah/configs"
	"golangSekolah/gu"
	"golangSekolah/models"
	"io/ioutil"
	"os"
)

func Load_Teacher()  {
	var (
		db, sql 		= configs.Connection()
		list 			[]models.Teacher
		file    		*os.File
		worker  		[]byte
		err 			error
	)

	defer gu.CloseDB(sql)
	if file, err = os.Open("seed/teacher.json");err != nil {
		gu.LogPrint(err)
		return 
	}

	if worker, err = ioutil.ReadAll(file);err != nil {
		gu.LogPrint(err)
		return
	}

	if err = json.Unmarshal(worker, &list);err != nil {
		gu.LogPrint(err)
		return
	}

	if err = db.Exec("DROP TABLE teacher_course").Error ;err != nil {
		gu.LogPrint(err)
		return 
	}
	if err = db.Exec("DROP table teachers").Error ;err != nil {
		gu.LogPrint(err)
		return 
	}
	if err = db.Exec("DROP TABLE courses").Error ;err != nil {
		gu.LogPrint(err)
		return 
	}
	
	models.Migration()
	if err = db.Create(&list).Error ;err != nil {
		gu.LogPrint(err)
		return 
	}

	if worker, err = json.MarshalIndent(list, "", "");err != nil {
		gu.LogPrint("Successfully load seed")
		gu.LogPrint(err)
		return
	}

	gu.LogPrint("Successfully load seed")
	gu.LogPrint(string(worker))

	db.Exec("ALTER SEQUENCE follow_ups_id_seq RESTART 5")
}

func LoadAll()  {
	Load_Teacher()
}