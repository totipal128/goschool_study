package models

import "golangSekolah/configs"

type Teacher struct{
	Model
	Name			string 	`json:"name"`
	Tittle 			string 	`json:"tittle"`
	LastEducation	string	`json:"last-education"`
	PlaceBirth		string	`json:"plalce_birth"`
	DateBirth		string 	`json:"date_birth"`
	Address			string	`json:"address"`
	NumberPhone		string	`json:"number_phone"`
	Course 			[]Course `gorm:"many2many:teacher_course;"`
}

func (this *Teacher)GetAll(value *[]Teacher)(err error)  {
	DB, sql := configs.Connection()
	defer sql.Close()

	//Preload = 

	err = DB.Preload("Course").Model(&this).Find(value).Error
	return
}

func (this *Teacher)GetDetail()(err error)  {
	DB, sql := configs.Connection()
	defer sql.Close()

	err = DB.First(this, "id = ?", this.ID).Error
	return
}

func (this *Teacher)Create()(err error)  {
	DB, sql := configs.Connection()
	defer sql.Close()

	err = DB.Create(&this).Error
	return
}

func (this *Teacher)Update()(err error)  {
	DB, sql := configs.Connection()
	defer sql.Close()

	err = DB.Updates(&this).Error
	return
}

func (this *Teacher)Delete()(err error)  {
	DB, sql := configs.Connection()
	defer sql.Close()

	err = DB.Delete(Teacher{}, "id  = ?", this.ID).Error
	return
}


//===============func relation teacher to course============================

type TeacherCourse struct{
	TeacherID  uint		`json:"teacher_id"`
	CourseID  uint		`json:"course_id"`
}

// func (this *TeacherCourse)DeleteCourse()(err error)  {
// 	DB, sql := configs.Connection()
// 	defer sql.Close()

// 	err = DB.Exec("DELETE FROM users").Error
// 	return
// }