package routes

import (
	"golangSekolah/controllers"

	"github.com/gin-gonic/gin"
)



func RouteStudent(rg *gin.RouterGroup)  {
	rg.GET("/", controllers.Hello)
	rg.GET("/view", controllers.GetAllStudents)
	rg.GET("/detail", controllers.GetDetailsStudent)
	rg.POST("/create", controllers.CreateStudent)
	rg.PUT("/update", controllers.UpadateStudent)
	rg.DELETE("/delete", controllers.DeleteStudent)
}