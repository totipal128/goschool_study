package routes

import (
	"golangSekolah/controllers"

	"github.com/gin-gonic/gin"
)

func CourseRoute(rg *gin.RouterGroup)  {
	rg.GET("/", controllers.CourseHello)
	rg.GET("/view", controllers.CourseGetAll)
	rg.GET("/detail", controllers.CourseDetail)
	rg.POST("/create", controllers.CourseCreate)
	rg.PUT("/update", controllers.CourseUpdate)
	rg.DELETE("/delete", controllers.CourseDelete)
}