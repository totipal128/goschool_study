package models

import "golangSekolah/configs"

type Course struct{
	Model
	NameCourse 		string 		`json:"name_course"`
	TeacherID		uint		`json:"teacher_id"`
}

func (this *Course)GetAll(value *[]Course)(err error)  {
	DB, sql := configs.Connection()
	defer sql.Close()

	//Preload = 

	err = DB.Model(&this).Find(value).Error
	return
}

func (this *Course)GetDetail()(err error)  {
	DB, sql := configs.Connection()
	defer sql.Close()

	err = DB.First(this, "id = ?", this.ID).Error
	return
}

func (this *Course)Create()(err error)  {
	DB, sql := configs.Connection()
	defer sql.Close()

	err = DB.Create(&this).Error
	return
}

func (this *Course)Update()(err error)  {
	DB, sql := configs.Connection()
	defer sql.Close()

	err = DB.Updates(&this).Error
	return
}

func (this *Course)Delete()(err error)  {
	DB, sql := configs.Connection()
	defer sql.Close()

	err = DB.Delete(Course{}, "id  = ?", this.ID).Error
	return
}