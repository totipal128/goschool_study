package models

import "golangSekolah/configs"

type Student struct{
	Model
	Name 		string 	`json:"name"`
	PlaceBirth	string 	`json:"place_birth"`
	DateBirth	string 	`json:"date_birth"`
	Gender 		string	`json:"gender"`
	Address 	string	`json:"address"`
	NumnerPhone string	`json:"number_phone"`
}

func (this Student)GetAll()(value []Student,err error)  {
	DB, sql := configs.Connection()
	defer sql.Close()

	err = DB.Model(&this).Find(&value).Error
	return value, nil
}

func (this *Student)GetDetail()(err error)  {
	DB, sql := configs.Connection()
	defer sql.Close()

	err = DB.First(this,"id = ?", this.ID).Error
	return
}

func (this *Student)Create()(err error)  {
	DB, sql := configs.Connection()
	defer sql.Close()

	err = DB.Create(&this).Error
	return
}

func (this *Student)Update()(err error)  {
	DB, sql := configs.Connection()
	defer sql.Close()

	err = DB.Updates(this).Error
	return
}

func (this *Student)Delete()(err error)  {
	DB, sql := configs.Connection()
	defer sql.Close()

	err = DB.Delete(Student{},"id = ?", this.ID).Error
	return
}