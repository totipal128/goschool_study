package configs

import (
	"database/sql"
	"fmt"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

const(
	DB_EnvHost 		="localhost"
	DB_EnvUser 		="postgres"
	DB_EnvPass 		="12345678"
	DB_EnvPort 		="5432"
	DB_EnvSSlMode 	="disable"
	DB_EnvTmz 		="Asia/Jakarta"

	DB_EnvName 		="goSchools"

)

var(
	DB_Host, 
	DB_User, 
	DB_Password, 
	DB_Port, 
	DB_Name,
	DB_SSl, 
	DB_Tmz string
)

func init()  {
	DB_Host 		= GetEnv("DB_HOST", DB_EnvHost)
	DB_User 		= GetEnv("DB_USER", DB_EnvUser)
	DB_Password 	= GetEnv("DB_USER", DB_EnvPass)
	DB_Name			= GetEnv("DB_NAME", DB_EnvName)
	DB_Port			= GetEnv("DB_NAME", DB_EnvPort)
	DB_SSl			= GetEnv("DB_SSL", DB_EnvSSlMode)
	DB_Tmz			= GetEnv("DB_SSL", DB_EnvTmz)
}

func Connection()(DB *gorm.DB, sql *sql.DB )  {
	var(
		dsn = fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=%s TimeZone=%s",DB_Host, DB_User, DB_Password, DB_Name ,DB_Port, DB_SSl, DB_Tmz )
		err error
	)
	if DB, err = gorm.Open(postgres.Open(dsn), &gorm.Config{}) ;err != nil {
		fmt.Println("Connection to Databases Failed", err.Error())
		panic(err.Error())
	}

	if sql, err = DB.DB() ;err != nil {
		fmt.Println("Connection to Databases Failed", err.Error())
		panic(err.Error())
	}

	fmt.Println("connection Berhasil")
	return
}