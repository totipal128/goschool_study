package controllers

import (
	"golangSekolah/models"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func CourseHello(c *gin.Context)  {
	c.JSON(200, gin.H{
		"message": "in from page Course",
	})
}

func CourseGetAll(c *gin.Context)  {
	var(
		data 	models.Course
		val 	[]models.Course
		err 	error
	)
	
	if err = data.GetAll(&val);err != nil {
		c.SecureJSON(http.StatusBadRequest, gin.H{
			"status"		: http.StatusBadRequest,
			"message"		: "Data Not Found",
			"data"			: nil,
		})
		return
	}

	c.SecureJSON(http.StatusOK, gin.H{
		"status"		: http.StatusOK,
		"message"		: "data retrieved successfully",
		"data"			: val,
	})
}

func CourseDetail(c *gin.Context)  {
	var(
		data 	models.Course
		id 		int64
		err		error
	)

	if value, ok := c.GetQuery("id"); ok {
		if id, err = strconv.ParseInt(value, 10, 64) ;err != nil {
			c.SecureJSON(http.StatusNotAcceptable, gin.H{
				"status" 	: http.StatusNotAcceptable,
				"data" 		: err.Error(),
			})
			return
		}
		data.ID = uint(id)
	}else{
		if err = c.ShouldBindJSON(&data) ;err != nil {
			c.SecureJSON(http.StatusUnauthorized, gin.H{
				"message" 	: err.Error(),
			})
			return
		}
	}

	if err = data.GetDetail();err != nil {
		c.SecureJSON(http.StatusBadRequest, gin.H{
			"status" 		: http.StatusBadRequest,
			"message"		: "data not found",
			"data"			: nil,
		})
		return 
	}

	c.SecureJSON(http.StatusOK, gin.H{
		"status" 		: http.StatusOK,
		"message"		: "Success",
		"data"			: data,
	})
}

func CourseCreate(c *gin.Context)  {
	var(
		data 	models.Course
		err 	error
	)

	if err = c.ShouldBindJSON(&data);err != nil {
		c.SecureJSON(http.StatusUnauthorized, gin.H{
			"status" 	: http.StatusUnauthorized,
		})
		return 
	}

	if err = data.Create();err != nil {
		c.SecureJSON(http.StatusBadRequest, gin.H{
			"status" 	: http.StatusBadRequest,
			"message"	: "Create Failed",
			"data" 		: nil,
		})
		return
	}

	c.SecureJSON(http.StatusOK, gin.H{
		"status" 	: http.StatusOK,
		"message"	: "Create Successfully",
		"data" 		: data,
	})
}

func CourseUpdate(c *gin.Context){
	var(
		data 	models.Course
		err 	error
	)

	if err = c.ShouldBindJSON(&data);err != nil {
		c.SecureJSON(http.StatusUnauthorized, gin.H{
			"message" 	: err.Error(),
		})
		return 
	}

	if err = data.Update();err != nil {
		c.SecureJSON(http.StatusBadRequest, gin.H{
			"status" 		: http.StatusBadRequest,
			"message"		: "Updates Failed",
			"data"			: nil,
		})
		return
	}

	c.SecureJSON(http.StatusOK, gin.H{
		"status" 		: http.StatusOK,
		"message"		: "Updates Success",
		"data"			: data,
	})
}

func CourseDelete(c *gin.Context)  {
	var(
		data 	models.Course
		id 		int64
		err 	error
	)

	if value, ok := c.GetQuery("id"); ok {
		if id, err = strconv.ParseInt(value, 10, 64) ;err != nil {
			c.SecureJSON(http.StatusNotAcceptable, gin.H{
				"status" 	: http.StatusNotAcceptable,
				"data" 		: err.Error(),
			})
			return
		}
		data.ID = uint(id)
	}else{
		if err = c.ShouldBindJSON(&data) ;err != nil {
			c.SecureJSON(http.StatusUnauthorized, gin.H{
				"message" 	: err.Error(),
			})
			return
		}
	}

	if err = data.GetDetail();err != nil {
		c.SecureJSON(http.StatusBadRequest, gin.H{
			"status" 		: http.StatusBadRequest,
			"message"		: "Deleted Failed",
			"data"			: nil,
		})
		return
	}

	if err = data.Delete();err != nil {
		c.SecureJSON(http.StatusBadRequest, gin.H{
			"status" 		: http.StatusBadRequest,
			"message"		: "Deleted Failed",
			"data"			: nil,
		})
		return
	}

	c.SecureJSON(http.StatusOK, gin.H{
		"status" 		: http.StatusOK,
		"message"		: "Deleted Success",
		"data"			: data,
	})
}