package main

import (
	"flag"
	"fmt"
	"golangSekolah/models"
	"golangSekolah/routes"
	"golangSekolah/seed"
	"log"
	"net/http"
	"time"

	"github.com/getsentry/sentry-go"
)

var (
	hostname, port, addr, location string
	load bool
)
func init()  {
	flag.StringVar(&hostname, "h", "0.0.0.0", "Hostname IP / domain")
	flag.StringVar(&port, "p", "7000", "Default port 8000")
	flag.StringVar(&location, "f", "./seeds/report_type.json", "Default on folder seed")
	flag.BoolVar(&load, "l", false, "Default load false")
	flag.Parse()
	addr = fmt.Sprintf("%s:%s", hostname, port)

	models.Migration()
}

func main()  {
	// fmt.Println(configs.Connection())
	err := sentry.Init(sentry.ClientOptions{
		BeforeSend: func(event *sentry.Event, hint *sentry.EventHint) *sentry.Event {
			if hint.Context != nil {
				if req, ok := hint.Context.Value(sentry.RequestContextKey).(*http.Request); ok {
					// You have access to the original Request
					fmt.Println(req)
				}
			}
			fmt.Println(event)
			return event
		},
		Debug:            true,
		AttachStacktrace: true,
	})

	if err != nil {
		log.Fatalf("sentry.Init: %s", err)
	}

	if load {
		time.Sleep(5 * time.Second)
		seed.LoadAll()
		models.Migration()
		return
	}

	routes.RouteServer().Run(addr)
}