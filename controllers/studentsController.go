package controllers

import (
	"golangSekolah/models"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func Hello(c *gin.Context)  {
	c.SecureJSON(http.StatusOK, gin.H{
		"messsage" 		: "in from data Students",
	})
}

func GetAllStudents(c *gin.Context)  {
	value, err := models.Student{}.GetAll()

	if err != nil {
		c.SecureJSON(http.StatusBadRequest, 
			gin.H{
				"message" 		: "data Not Found",
		})
		return
	}

	c.SecureJSON(http.StatusOK, gin.H{
		"status"  		: http.StatusOK,
		"message"		: "succcess",
		"data" 			: value,
	})
}

func GetDetailsStudent(c *gin.Context)  {
	var (
		data 	models.Student
		id 		int64
		err		error
	)

	if value, ok := c.GetQuery("id"); ok {
		if id, err = strconv.ParseInt(value, 10, 64) ;err != nil {
			c.SecureJSON(http.StatusNotAcceptable, gin.H{
				"status" 	: http.StatusNotAcceptable,
				"data" 		: err.Error(),
			})
			return
		}
		data.ID = uint(id)
	}else{
		if err = c.ShouldBindJSON(&data) ;err != nil {
			c.SecureJSON(http.StatusUnauthorized, gin.H{
				"message" 	: err.Error(),
			})
			return
		}
	}

	if err = data.GetDetail();err != nil {
		c.SecureJSON(http.StatusBadRequest, gin.H{
			"status"	: http.StatusBadRequest,
			"message"	: "data not found",
			"data"		: nil,
		})
		return
	}

	c.SecureJSON(http.StatusOK, gin.H{
		"status"	: http.StatusOK,
		"message" 	: "success",
		"data" 		: data,
	})
}

func CreateStudent(c *gin.Context)  {
	var(
		data 	models.Student
		err 	error
	)

	if err = c.ShouldBindJSON(&data) ;err != nil {
		c.SecureJSON(http.StatusUnauthorized, gin.H{
			"message" 	: err.Error(),
		})
		return
	}

	if err = data.Create();err != nil {
		c.SecureJSON(http.StatusBadRequest, gin.H{
			"status"		: http.StatusBadRequest,
			"message"		: "Create failed",
			"data"			: nil,
		})
		return 
	}

	if err = data.GetDetail();err != nil {
		c.SecureJSON(http.StatusBadRequest, gin.H{
			"stattus"		: http.StatusBadRequest,
			"message"		: "Data not Found",
			"data"			: nil,
		})
		return
	}

	c.SecureJSON(http.StatusOK, gin.H{
		"stattus"		: http.StatusOK,
		"message"		: "Success",
		"data"			: data,
	})
}

func UpadateStudent(c *gin.Context)  {
	var(
		data 	models.Student
		err		error
	)

	if err = c.ShouldBindJSON(&data);err != nil {
		c.SecureJSON(http.StatusUnauthorized, gin.H{
			"message" 	: err.Error(),
		})
		return
	}

	if err = data.Update(); err != nil {
		c.SecureJSON(http.StatusBadRequest, gin.H{
			"status" 		: http.StatusBadRequest,
			"message" 		: "Updates Failed",
			"data"			: nil,
		})
		return
	}

	if err = data.GetDetail(); err != nil {
		c.SecureJSON(http.StatusBadRequest, gin.H{
			"status" 		: http.StatusBadRequest,
			"message" 		: "Updates Failed",
			"data"			: nil,
		})
		return 
	}

	c.SecureJSON(http.StatusOK, gin.H{
		"status" 		: http.StatusOK,
		"message" 		: "Update Success",
		"data"			: data,
	})
}

func DeleteStudent(c *gin.Context)  {
	var (
		data 	models.Student
		id 		int64
		err		error
	)

	if value, ok := c.GetQuery("id"); ok {
		if id, err = strconv.ParseInt(value, 10, 64) ;err != nil {
			c.SecureJSON(http.StatusNotAcceptable, gin.H{
				"status" 	: http.StatusNotAcceptable,
				"data" 		: err.Error(),
			})
			return
		}
		data.ID = uint(id)
	}else{
		if err = c.ShouldBindJSON(&data) ;err != nil {
			c.SecureJSON(http.StatusUnauthorized, gin.H{
				"message" 	: err.Error(),
			})
			return
		}
	}

	if err = data.GetDetail();err != nil {
		c.SecureJSON(http.StatusBadRequest, gin.H{
			"status"	: http.StatusBadRequest,
			"message" 	: "Data Not Found",
			"data"		: nil,
		})
		return 
	}

	if err = data.Delete();err != nil {
		c.SecureJSON(http.StatusBadRequest, gin.H{
			"status" 	: http.StatusBadRequest,
			"message" 	: "Deleted failed",
			"data"		: nil,
		})
		return
	}

	c.SecureJSON(http.StatusOK, gin.H{
		"status" 	: http.StatusOK,
		"message" 	: "Deleted Success",
		"data"		: data,
	})
}