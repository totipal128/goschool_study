package models

import "golangSekolah/configs"

func Migration()  {
	db,_ := configs.Connection()
	
	db.AutoMigrate(
		&Student{},
		&Teacher{},
		&Course{},
	)
}