package configs

import "os"


func GetEnv(key, default_val string) string {
	value, exist := os.LookupEnv(key)
	if !exist {
		return default_val
	}
	return value
}	
