package routes

import (
	"golangSekolah/controllers"

	"github.com/gin-gonic/gin"
)

func TeacherRoutes(rg *gin.RouterGroup)  {
	rg.GET("/", controllers.TeacherHello)
	rg.GET("/view", controllers.TeacherGetAll)
	rg.GET("/detail", controllers.TeacherDetail)
	rg.POST("/create", controllers.TeacherCreate)
	rg.PUT("/update", controllers.TeacherUpdate)
	rg.DELETE("/delete", controllers.TeacherDelete)
}