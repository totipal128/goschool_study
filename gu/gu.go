package gu

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

// GetEnvString for get env type string
func GetEnvString(key, defaultVal string) string {
	value, exist := os.LookupEnv(key)
	if !exist {
		return defaultVal
	}
	return value
}

// GetEnvBool for get env type bool
func GetEnvBool(key string, val bool) bool {
	value, exist := os.LookupEnv(key)
	if !exist || value == "" {
		return val
	}

	b, err := strconv.ParseBool(value)
	if err != nil {
		return val
	}
	return b
}

// CloseDB for secure close sql DB with message error if error
func CloseDB(sql *sql.DB) {
	PrettyPrint(sql.Close())
}

// CloseBody for secure close io ReadCloser with message error if error
func CloseBody(Body io.ReadCloser) {
	ErrorHandler(Body.Close())
}

// CloseFile for secure close os file with message error if error
func CloseFile(file *os.File) {
	ErrorHandler(file.Close())
}

// EqualString for cek if string contain substring
// example : EqualString("Report Service", "report") will return true.
func EqualString(str, subStr string) bool {
	if ok := strings.Index(strings.ToLower(str), strings.ToLower(subStr)); ok > -1 {
		return true
	}

	return false
}

// PrettyPrint for print struct/map/array to console as indent view
func PrettyPrint(data interface{}) {
	if data != nil {
		p, err := json.MarshalIndent(data, "", "\t")
		if err != nil {
			PrettyPrint(err)
			return
		}
		fmt.Printf("[%s]\n%s \n", GetEnvString("ENGINE_NAME", "UNDEFINED"), p)
	}
}

// ErrorHandler for print error message to console with engine name if error != nil
func ErrorHandler(err error) {
	if err != nil {
		fmt.Printf("[%s] %s\n", GetEnvString("ENGINE_NAME", "UNDEFINED"), err.Error())
	}
}

// LogPrint for print message for debug with engine name
func LogPrint(args ...interface{}) {
	if true {
		fmt.Printf("[%s] ", GetEnvString("ENGINE_NAME", "UNDEFINED"))
		_, _ = fmt.Fprintln(os.Stdout, args...)
	}
}

// Convert for convert from map[string]interface{} to struct or reverse or array of them
func Convert(source interface{}, destination interface{}) {
	var (
		jsonBody	[]byte
		err 		error
	)
	if jsonBody, err = json.Marshal(source); err != nil {
		ErrorHandler(err)
	}
	if err = json.Unmarshal(jsonBody, destination); err != nil {
		ErrorHandler(err)
	}
}

//StringToUintArr for convert array of string number to unit array
func StringToUintArr(req []string) (res []uint) {
	for _, val := range req {
		num, _ := strconv.Atoi(val)

		res = append(res, uint(num))
	}
	return
}