package routes

import (
	"net/http"

	sentrygin "github.com/getsentry/sentry-go/gin"

	"github.com/gin-gonic/gin"
)

func RouteServer()(r *gin.Engine)  {
	gin.ForceConsoleColor()
	r = gin.New()

	//Middleware
	r.Use(gin.Logger())
	r.Use(gin.Recovery())
	r.Use(sentrygin.New(sentrygin.Options{
		Repanic: true,
	}))

	r.Use(func(ctx *gin.Context) {
		if hub := sentrygin.GetHubFromContext(ctx); hub != nil {
			hub.Scope().SetTag("someRandomTag", "maybeYouNeedIt")
		}
		ctx.Next()
	})

	//root
	r.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "Data Sekolah",
		})
	})

	parent := r.Group("/schools")
	
	hei := parent.Group("/hei")
	student 	:= parent.Group("/student")
	teacher 	:= parent.Group("/teacher")
	course 		:= parent.Group("/course")

	Hello(hei)
	RouteStudent(student)
	TeacherRoutes(teacher)
	CourseRoute(course)

	return
}

func Hello(rg *gin.RouterGroup)  {
	rg.GET("/", func(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
			"message": "Hei",
		})
	})
}